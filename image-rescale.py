#!/usr/bin/env python3

import cv2
from PIL import Image, ImageDraw, ImageFilter, ImageFont
import numpy
import sys

if (len(sys.argv) < 3):
    print('Script requires 2 params: input_file.png output.png\nthird parameter is optional blur pixels')
    exit()

imagePath = sys.argv[1]
outPath = sys.argv[2]

inclusion_blur = 40
background_blur = 40

if (len(sys.argv) > 3):
    inclusion_blur = int(sys.argv[3])
    background_blur = int(sys.argv[3])


# 1080 x 1350 4/5
target_width = 1080
target_height = 1350

# Pillloow
im_rgb_original = Image.open(imagePath).convert('RGBA')

(original_width, original_height) = im_rgb_original.size

x_offset = ((original_height / 5 * 4) - original_width) / 2

im_rgb = im_rgb_original.transform((target_width, target_height),
    Image.Transform.EXTENT,
    (
        -x_offset,
        0,
        original_width + x_offset,
        original_height)
)

im_rgb_background = im_rgb_original.transform((target_width, target_height),
    Image.Transform.EXTENT,
    (
        0,
        0,
        original_width,
        original_height)
)

im_a = Image.new("L", im_rgb.size, 0)
draw = ImageDraw.Draw(im_a)

rescaled_width = target_height * (original_width / original_height)
draw.rectangle(
    [
        ((target_width - rescaled_width) / 2 + (inclusion_blur * 2), 0),
        ((target_width - rescaled_width) / 2 + rescaled_width - (inclusion_blur * 2), target_height)
    ], fill = 255
)

im_a_blur = im_a.filter(ImageFilter.GaussianBlur(inclusion_blur))
im_rgba = im_rgb.copy()
im_rgba.putalpha(im_a_blur)


im_rgb_blured = im_rgb_background.filter(ImageFilter.GaussianBlur(background_blur))
im_rgb_blured.alpha_composite(im_rgba)

im_rgb_blured.save(outPath)
