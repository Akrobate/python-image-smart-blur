#!/usr/bin/env python3

import cv2
from PIL import Image, ImageDraw, ImageFilter, ImageFont
import numpy
import sys
import os

if (len(sys.argv) < 3):
    print('Script requires 2 params: input_file.png output.png')
    exit()

imagePath = sys.argv[1]
outPath = sys.argv[2]

face_inclusion_blur = 30
background_blur = 30

text_box_margin = 15

# 1080 x 1350 4/5
target_width = 1080
target_height = 1350

# Text contents
text_1_content = "Unblured on Telegram"
text_2_content = "link in bio"
font_logo_char = 'O'

try:
    text_1_content = sys.argv[3]
    text_2_content = sys.argv[4]
    font_logo_char = sys.argv[5]
except IndexError:
    pass


script_base_path = os.path.dirname(os.path.abspath(__file__))


def getFacesFromPilImage(pil_image):
    pil_rgb_img = pil_image.convert('RGB')
    open_cv_image = numpy.array(pil_image) 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 

    gray_image = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)
    face_classifier = cv2.CascadeClassifier(
        cv2.data.haarcascades + "haarcascade_frontalface_default.xml"
    )
    face = face_classifier.detectMultiScale(
        gray_image, scaleFactor=1.1, minNeighbors=5, minSize=(40, 40)
    )
    result = []
    for (x, y, w, h) in face:
        result.append((x, y, x + w, y + h))
    return result


def addTextToDraw(draw, font_text, text_content, text_position, box_margin):
    left, top, right, bottom = draw.textbbox(text_position, text_content, font=font_text)
    draw.rectangle(
        (left-box_margin, top-box_margin, right+box_margin, bottom+box_margin),
        fill=(0, 0, 0, 128)
    )
    draw.text(
        text_position,
        text_content,
        fill=(150, 150, 150, 256),
        font=font_text
    )

# Pillloow
im_rgb = Image.open(imagePath).convert('RGBA')
(original_width, original_height) = im_rgb.size

x_offset = ((original_height / 5 * 4) - original_width) / 2

im_rgb = im_rgb.transform((target_width, target_height),
    Image.Transform.EXTENT,
    (
        -x_offset,
        0,
        original_width + x_offset,
        original_height)
)

# Applying black background
im_background = Image.new("RGBA", im_rgb.size, (0,0,0,255))
im_background.alpha_composite(im_rgb)
im_rgb = im_background

face = getFacesFromPilImage(im_rgb)

image_alpha_monochannel = Image.new("L", im_rgb.size, 0)
draw = ImageDraw.Draw(image_alpha_monochannel)

for (x0, y0, x1, y1) in face:
    add_face_margin = (x1 - x0) / 2
    draw.ellipse(
        (
            x0 - add_face_margin,
            y0 - add_face_margin,
            x1 + add_face_margin,
            y1 + add_face_margin
        ),
        fill=255
    )

image_alpha_monochannel_blured = image_alpha_monochannel.filter(
    ImageFilter.GaussianBlur(face_inclusion_blur)
)
im_rgba = im_rgb.copy()
im_rgba.putalpha(image_alpha_monochannel_blured)

im_rgb_blured = im_rgb.filter(ImageFilter.GaussianBlur(background_blur))



font_text_1 = ImageFont.truetype(script_base_path + '/fonts/mensch.ttf', 60)
font_text_2 = ImageFont.truetype(script_base_path + '/fonts/mensch.ttf', 50)
font_logo = ImageFont.truetype(script_base_path + '/fonts/Social Logos.ttf', 150)

text_background_layer = Image.new("RGBA", im_rgb_blured.size)
Im = ImageDraw.Draw(text_background_layer, "RGBA")

text_1_position = (60, 950)
addTextToDraw(Im, font_text_1, text_1_content, text_1_position, text_box_margin)

text_2_position = (180, 1040)
addTextToDraw(Im, font_text_2, text_2_content, text_2_position, text_box_margin)

Im.text((60, 995), font_logo_char, fill=(0, 136, 204, 256), font=font_logo)

im_rgb_blured.alpha_composite(im_rgba)

#text_background_layer=text_background_layer.rotate(17.5,  expand=1)
im_rgb_blured.alpha_composite(text_background_layer)

im_rgb_blured.save(outPath)
