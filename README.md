# python-image-smart-blur

## About script

* input image can be in all resolution
* input image can be in png with transparency or jpg
* Transparency will be replaced with black background
* output ratio is 4/5
* output resolution is 1080px per 1350px


## Install

```sh
pip install virtualenv
python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Requirements

* python3
* python opencv
* pillow
* numpy

### create virtualenv

```sh
python3 -m virtualenv venv
```

### activate virtualenv

```sh
source venv/bin/activate
```

### run scripts

This script will blur all the image and keep the face unblured,
Some text can be added for call to action

```sh
# ./image-blur.py input_file.jpg output_file.jpg
./image-blur.py test_images/joconde.jpg output.png
```

Image blur with custom texts
```sh
# ./image-blur.py input_file.jpg output_file.jpg [First line] [First line] [font icon]
./image-blur.py test_images/joconde.jpg output.png "Unblured on Telegram" "link in bio" "O"
```


This script will rescale the image to be in format of 4/5. The background will be deformed and blured

```sh
# ./image-blur.py input_file.jpg output_file.jpg [BlurPixels(Optional)]
./image-rescale.py test_images/joconde.jpg output.png 50
./image-rescale.py test_images/joconde.jpg output.png
```


In PROGRESS:

Adds a watermark to the image

```sh
./image-watermark.py test_images/joconde.jpg output.png 50
```

